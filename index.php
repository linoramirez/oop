<?php include 'class/House.php';?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Curso de Programación Orientada a Objetos (POO)</title>
  </head>
  <body>
    <h1>Ejercicio 1 - Definir un objeto con propiedades y metodo</h1>
    <h3>Instrucciones:</h3>
    <h4>
      <ol>
        <li>Realiza una definición de algún objeto de tu preferencia, por ejemplo: Un carro. Tengo un Toyota de color rojo, con cuatro puertas el cual puede acelerar y tocar la bocina.</li>
        <li>A partir del ejercicio anterior, procede a crear una Clase con las propiedades y los métodos que consideres que estén dentro de tu definición.</li>
      </ul>
      </h4>
      <p><?php $miCasa = new House("Lino Ramirez", "Fracc. Villa del Cedro", 2, "beiche", "Si");
                    echo $miCasa->mostrarInfo();
              ?>
      </p>
      <p>
        <?php $tiaCasa = new House("Mi Tia Ana", "Fracc. del Bosque", 2, "Blanca", "Si");
                    echo $tiaCasa->mostrarInfo();
              ?>
      </p>
      <p><?php $papasCasa = new House("Mis Papas", "Fracc. Villa Galaxia", 2, "Cafe", "Si");
                  echo $papasCasa->mostrarInfo();
             ?>
      </p>

  </body>
</html>
