<?php
/**
 *
 */
class House //extends AnotherClass
{
  var $propietario;
  var $domicilio;
  var $plantas;
  var $color;
  var $cochera;

   function   __construct($propietario, $domicilio, $plantas, $color, $cochera){
      $this->propietario = $propietario ;
      $this->domicilio = $domicilio ;
      $this->plantas = $plantas ;
      $this->color = $color;
      $this->cochera = $cochera;
  }

  function mostrarInfo(){
      Return "La Casa de {$this->propietario} se encuentra ubicada en {$this->domicilio}, es de color {$this->color} tiene {$this->plantas} planta(s) y {$this->cochera} cochera(s).";
  }

}


?>
